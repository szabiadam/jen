package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static AngajatiApp.controller.DidacticFunction.LECTURER;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class EmployeeMockTest {

    private EmployeeMock cr;

    @Before
    public void setUp() throws Exception {
        cr=new EmployeeMock();
        Employee c = new Employee("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        Employee c1 = new Employee();
        cr.addEmployee(c);
        cr.addEmployee(c1);
    }

    @After
    public void tearDown() throws Exception {
        cr=null;
    }

    @Test
    public void modifyEmployeeFunctionTC1() {

        List<Employee> l=cr.getEmployeeList();
        for(Employee c : l)
            if(c.getId() == 0){
                cr.modifyEmployeeFunction(c, LECTURER);
                assertEquals(LECTURER,c.getFunction());
                assertEquals(true, c.getFunction()==LECTURER);}
    }

    @Test
    public void modifyEmployeeFunctionTC2() {
        Employee employee = new Employee(null, null, null, null, null);
        cr.modifyEmployeeFunction(employee, LECTURER);
                assertNotEquals(LECTURER,employee.getFunction());
    }

    @Test
    public void modifyEmployeeFunctionTC3() {
        cr=new EmployeeMock();
        Employee c = new Employee("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        cr.modifyEmployeeFunction(c,LECTURER);
        Assert.assertNotEquals(LECTURER, c.getFunction());

    }

    @Test
    public void modifyEmployeeFunctionTC4() {
        EmployeeRepositoryInterface employee = new EmployeeMock();
        Employee c = new Employee("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        c.setId(99);
        employee.modifyEmployeeFunction(c,LECTURER);
        Assert.assertNotEquals(LECTURER, c.getFunction());
    }

//    @Test
//    public void TC3() {
//
//        Employee c=new Employee();
//
//        c.setFirstName("Marius");
//
//        ArrayList<String> a=new ArrayList<String>();
//
//        c.setLastName("Pacurar");
//
//        c.setCnp("1234567890876");
//
//        c.setFunction(ASISTENT);
//
//        c.setSalary(1d);
//
//        assertEquals(true,cr.addEmployee(c));
//
//    }
//
//    @Test
//    public void TC4() {
//
//        Employee c=new Employee();
//
//        c.setFirstName("Marius");
//
//        ArrayList<String> a=new ArrayList<String>();
//
//        c.setLastName("Pacurar");
//
//        c.setCnp("1234567890876");
//
//        c.setFunction(ASISTENT);
//
//        c.setSalary(0d);
//
//        assertEquals(false,cr.addEmployee(c));
//
//    }

}